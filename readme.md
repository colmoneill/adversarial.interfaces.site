# adversarial . interfaces . site

<div class="row">
<div class="colspan12-3 colspan6-6 as-grid">
  <p>Adversarial interfaces aims to critically analyse the rhetoric portrayals that interfaces give way to. By this analysis, to practically and discursively augment our understandings of computer and network infrastructures by using critical methods to counter act seamless integration of our networked spaces as totalities that have no outside.</p>
</div>
<div class="colspan12-3 colspan6-6 as-grid with-gutters">
  <p>This site portrays a frame of thought that appends the idea of Adversarial Design to user interfaces. Adversarial Design is described as design that does the work of agonism.</p>
  <p>The project exists as an expression of a need for agonism in the field of user interface design.</p>
  </div>
<div class="colspan12-5 colspan6-6 as-grid with-gutters">
  <p class="definition"><b>Adversarial design</b>: (Carl DiSalvo) a type of political design that evokes and engages political issues. Adversarial Design does the work in expressing and enabling agonism.</p>
  <p class="definition"><b>Agonism</b>: (Chantal Mouffe) a political theory that emphasises the potentially positive aspects of certain forms of political conflic.</p>
  <p class="definition"><b>Seamless interface:</b> two (or more) computer programs that are carefully joined together so that they appear to be a single program with a single user interface. A seamless interface will conceal the fact that it is created from two or more different programs.</p>
  <p class="definition"><b>the internet: </b> <a href="https://criticalengineering.org/">(Critical Engineers)</a> a deeply misunderstood set of technologies upon which we increasingly depend.</p>
</div>
</div>
<div class="row">
<div class="colspan12-5 colspan6-6">
<p><b>Adversarial interfaces</b> is a site in building. It's first itteration exists to mark a point at the finalisation of a masters degree in media design and communication by <a href="http://colm.be">Colm O'Neill</a>.</p>
</div>
<div class="colspan12-5 colspan6-6">
<p>To visually support the idea of agonism and adversivity, the <a href="https://en.wikipedia.org/wiki/Astrological_symbols#Aspects">astrological aspect symbol for oppostion</a> is employed. ☍ is described as “The glyph of the Conjunction plus a circle on top of its line, implying two objects are in front (opposed) of each other.” Detailing placings of 180° opposition.</p>
</div>
