Title: informal modes of address
Date: 2017/03
Tags: examples, gallery, modes of address

# Informal address in user interface

# examples

# direct address, sites calling me by my first name
![](../images/grad-pres/hi-colm-amazon.png)

![](../images/grad-pres/dont-worry-well-keep-it-private-crop.png)

![](../images/grad-pres/5057840351_f14f986073_b.jpg)

![](../images/grad-pres/tumblr-welcome-back.png)


# informality to obtain consent

![](../images/grad-pres/tumblr-consent-sure.png)

![](../images/grad-pres/twitter-sounds-good.png)

# informality to explain a point of view

![](../images/grad-pres/Screenshot_2017-02-13_12-32-02.png)

![](../images/grad-pres/daily-mash-crop.png)

![](../images/grad-pres/nothardtoexplain-crop.png)

# informality to express care

![](../images/grad-pres/memo2.jpg)

![](../images/grad-pres/splat-human-head.png)

# informality to detail a change or an error

![](../images/grad-pres/tumblr-date-thee.png)

![](../images/grad-pres/oh-fine.png)

![](../images/grad-pres/highly-trained-monkeys.png)

![](../images/grad-pres/digitalocean-awesome.png)
