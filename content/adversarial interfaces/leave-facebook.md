Title: why/how to leave facebook — Nick Briz
Date: 2014
Tags: method, artistic approach

# Why / How to leave Facebook
## http://nickbriz.com/facebook/

![](../images/nick-briz.png)

Nick Briz is a new media artist / educator / organiser from Chicago. The project “Why / How To Leave Facebook” is a multi medium (opinion) piece in which we see Nick detail the reasons for his decision to leave the social network. He explains how he went about *effectively* leaving Facebook. The artists position is that simply deleting ones account using the facebook-built deletion button is not sufficient, this type of deletion would correspond better to a sort of suspention in time, a point after which the ‘deleted’ user can not change the dataset, but nothing close to deleting the data itself.

The ‘how to’ section of this project materialises as a set of scripts to run in the console of a logged in facebook browser session. These scripts perform looped actions that enable the executor to deconstruct the tables of relations that consitute Facebooks user data. After a set of semi-manual backups of data that may be useful to revisit, browser scripts are ran in sequence to bit by bit, untag onself from all photos, delete all photos, leave all groups, delete all ones activity and finally unfriend all of ones friends. This process has a real dramatic dimention, one that is only slightly facilitated by running scripts to do the heavy (repetitive) lifting. This is also clearly an emotionally envolved process, and Nick does not try to hide that in any way. In fact, I believe a lot of what makes the videos (screencast/demo/presentations in which Nick details shows the processes) of this project so strong is the disbelief, grief and disenchantment that he feels and displays as he explains.

<iframe src="https://www.youtube.com/embed/JEeR9jUsiyo" frameborder="0" allowfullscreen></iframe>

The emotional aspect of this project aside for an instant, I find the methodology developped in order to leave facebook very well woven. Specifically, the logging of the functions that the buttons of the facebook interface run, the reverse engineering, if you will, of all of the recipes that build the front end interface is a real example of how to interact with the opposites of seamlessness interfaces. Nick describes these scripts as brute force hacks, and by that he means that the exploits he has found in the un-engineering process are lightly to be different for different situations. But actually, I believe he uses the words ‘brute force’ and ‘hack’ to warn anybody that may be reproducing some of his steps that there is an unofficial and unstable nature to the processes. Essentially, this project uses knowledge of how website interfaces are built, to unbuild it. He runs the same methods, the same keys, the same functions that the site itself runs, but simply in an unexpected (from Facebooks side) order.

As diheartening as it is that this project exists because an enthusiastic contributor of/to internet culture feels obligated to erase a portion of his online interactions, the methods divised point to alternative practices, circumventions and reorganisations that restore iotas of control to Nick, and let him leave correctly and on his terms.             
