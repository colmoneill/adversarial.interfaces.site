Title: adversarial design → adversarial interfaces; a refresh
Date: 2018/11
Writing: in progress
Status: draft

Adversarial design is design that does the work of agonism. Agonism is a political theory that emphasizes the potentially positive aspects of certain forms of political conflict. Adversarial interfaces do the work of agonism in multiple ways: it expresses bias and divisive positions; it provides opportunities to participate in disputes over values, believes and desires; and it models alternate socio-material configurations that demonstrate possible futures.

I draw heavily from DiSalvo in establishing Adversarial Interfaces. In fact I am not establishing it as he himself covers a few design projects that employ interfacing as a tool for agonism. I put adversarial interfaces forwards as a way to point the finger at interface design as a space that needs more critical thinking. I believe that the methods we are given to interact with ear shaping technology, the windows we are given to interact with do not enable proper exploration or understanding of the matters at hand.
