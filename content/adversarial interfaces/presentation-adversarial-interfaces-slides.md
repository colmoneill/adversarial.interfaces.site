Title: adversarial interfaces public presentation
Date: 2017/06/15
Template: slidy
Status: draft

# Adversarial interfaces

http://adversarial.interfaces.site

Graduate reseach project<br>
Colm O'Neill<br>
mail@colm.be<br>

<!-- digital ocean awesome popup -->
# ![](../images/grad-pres/digitalocean-awesome.png)

<!-- TUMBLR Cards img -->
# ![](../images/grad-pres/nothardtoexplain-crop.png)

<!-- WIRED ad block -->
# ![](../images/wired-the-thing-with-ad-blockers.png)

# ![](../images/twitter-sounds-good/Screenshot_from_2017-05-18_11-52-15.png)

# ![](../images/twitter-sounds-good/twitter-sounds-good.gif)

# ![](../images/real-life-mag-mismanaged-heart.png)

# <img class="vertical" src="../images/surveillance-capture.png">

# ![](../images/grad-pres/facebook.gif)

<!-- TEXT SLIDE: Seamless definition -->
# Seamless interfaces:

two (or more) computer programs that are carefully joined together so that they appear to be a single program with a single user interface. A seamless interface will conceal the fact that it is created from several different programs.

# ![](../images/tangible.tools.screen.png)

# ![](../images/internet-will-disappear.png)

# <img class="vertical" src="../images/JK-Gibson-Graham.jpg">

<!-- adversarial interfaces site -->
# ![](../images/adversarial-interfaces-site.png)

<!-- Text Slide: Adversarial Design, Agonism, Chantal Mouffe -->
# Adversarial design

Adversarial design: (Carl DiSalvo) a type of political design that evokes and engages political issues. Adversarial Design does the work in expressing and enabling agonism.

Agonism: (Chantal Mouffe) a political theory that emphasises the potentially positive aspects of certain forms of political conflic.
