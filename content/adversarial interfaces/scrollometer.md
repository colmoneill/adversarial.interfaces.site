Title: scrollometer
Date: 2016/05
Tags: webExtension, critical making

# Scrollometer

http://pzwart1.wdka.hro.nl/~colm/prototypes/scrollometer/

https://gitlab.com/colmoneill/WebExtensions/tree/master/scrollometer

Scrolling is an achievement! Be proud of your metrics!
How can we grasp physical world units on screens? How can I print a centimeter on the screen. How can I make sure the centimeter will remain a centimeter across all types of screens?

We know screens are just grids of pixels, but what is less easy is to find out how many pixels are on the panel. If you know the physical measurements of the screen, and the screen resolution in pixels, then we can work out the pixel density. Reminiscents of the print world call this number PPI, points per inch, in this case pixels per inch, effectively the same notion.
pixels per inch

If we know a screen's PPI, (in my case I'll rather a PP-centimeter) we can make rules to draw and display real centimeters. Keep in mind that this won't ever be perfectly precise, grids of pixels in pannels don't constrain to a rhythmical finish, and we can't exactly turn halfs or quarter pixels on or off.

use case: my personal laptop screen. My laptop has a 'wide screen' (13.6"x7.6"), and resolution is 1366x768 pixels, 1366 / 13.6 = 100.44 PPI. Granted, you could also simply search for your exact device online and discover your PPI this way
theoretical usage

So the rule would look like this

    in a css preprocessor variable definition sheet :
    var ppi = 100.44;
    var ppcm = (ppi * 2.54 /* 1 cm = 2.54 in */);
    var realcm = ppcm

    then in use :
    div.tenCm{
    width: (10*realcm) px;
    }

devicePixelRatio
vocabulary

notions that need defining:

    physical pixels
    dips, device independent pixels
    device pixel Ratio

references:
http://www.quirksmode.org/blog/archives/2012/06/devicepixelrati.html
http://www.quirksmode.org/blog/archives/2012/07/more_about_devi.html
