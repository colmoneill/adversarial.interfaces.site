Title: clickbait-stopper
Date: 2016/10/21
Tags: webExtension


# Stop clickbait

https://github.com/mrdoob/clickbait-stopper

Stop clickbait is a webextension that aims to detect and flag content that is considered Clickbait. The word clickbait is a qualifier given to links and titles of articles on websites that aim exclusively to get you to click. It is a type of sentence that is written to intrigue and grab your attention.

![](../images/forbes-clickbait-list.png)

While clickbait-stopper's strategy is to strike-through links that match patterns (as opposed to other project that allow you to dissapear the titles altogether), it's existance points to a very simple reality; content websites build clickbait titles following such a well known formula, that a simple bit of javascript, can actively flag titles that match the pattern.

Here is the list of patterns used by clickbait-stopper:

```
const list = [
	/^\d+ (\w+ )?(animals|pictures|lessons|movies|secrets|shows|stories|things|times|trailers|tumblr|tweets)/i,
	/^\d+ of the \w+est/i,
	/^can you/i,
	/^here['’]s (how|what)/i,
	/^this could/i,
	/all (he|she|they) did was/i,
	/all the best/i,
	/best comeback/i,
	/can['’]t handle/i,
	/can teach us about/i,
	/didn['’]t know what/i,
	/get rid of/i,
	/how one (man|woman)/i,
	/how \w+ are you/i,
	/may affect/i,
	/never realized/i,
	/(pictures|photos) of/i,
	/secret of/i,
	/signs you['’]?re/i,
	/somebody needs to/i,
	/things that will/i,
	/trump/i,
	/until you see/i,
	/you (need to|should) (know|watch)/i,
	/we bet you can/i,
	/we can (tell|guess) (what )?your/i,
	/we need to talk about/i,
	/what could possibly/i,
	/what happens/i,
	/what (he|she|they) found/i,
	/what I learned about/i,
	/what this/i,
	/what to expect/i,
	/when (he|she|they)/i,
	/when this (man|woman|baby|child|puppy|dog|kitten)/i,
	/when you read these/i,
	/who['’]d thougt/i,
	/why we really shouldn['’]?t/i,
	/with this one/i,
	/will never tell/i,
	/won['’]?t believe/i,
	/\s(celebrit|epic|fantastic|genius|heartbreaking|incredibl|powerful|shocking|teen|terribl|unusual|weirdly)/i
];
```

I find it very interesting that such a list of formulations can be brought together. Note also that one of the lines simply exists to cross out any title that mentions "trump". As I probe this extension to see it function, I can but feel somewhat confused.

![](../images/bbc-clickbait.png)

Take this example of clickbait-stopper running on the BBC's homepage. I suppose, firstly that indeed, the title *"Can you be taught to be more charismatic"* qualifies as an empty question who's organisation is meant to intrigue. If we, for the sake of argument, agree that this is clickbait, then what makes the title besides it *"Is 'the end of modern medecine' near?"* not clickbait?

What I'm interested in here, is the perspective of what is and what is not clickbait. When I install clickbait-stopper, I get one person's blacklist applied to my visits. I like to imagine what this extension might provoque, had [Mr.doob](https://github.com/mrdoob) decided that, instead of crossing out titles that matched his list, he had simply rendered them hidden.

It is not the subject matter that makes me propose this extension as adversarial interfaces, it is the decision to cross out, but keep legible the sentences, links and titles that match a pattern. This method of flagging and not disapearing is a brilliant method for *staying with a trouble* that is political. This extension shows disagreement and enforces the notion that, while the website does contain this title, and that the sentence was designed to divert one's natural navigation of a website, the browser is a local interpreter of online resources. And as a local resource, it can be vandalistic, and it can abide to one's ideals, and ones resitances.
