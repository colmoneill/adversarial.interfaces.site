Title: slides Universe of () images symposium
Date: 2018/11/16
Template: slidy
Status: published

<div class="slide section level1">

<p>Title: Universe of () images symposium Date: 2018/11/16 Template: slidy Status: draft</p>
</div>
<div id="introduction" class="slide section level1">
<h1>introduction</h1>
<h2 id="adversarial-interfaces">adversarial interfaces</h2>
<h3 id="universe-of-images">universe of () images</h3>
<h3 id="section">2018/11/23</h3>
<p>Colm O’Neill<br> <a href="colm.be">colm.be</a><br> <a href="osp.kitchen">osp.kitchen</a><br></p>
<p>mail@colm.be<br> <span class="citation">@colm</span><span class="citation">@mastodon.cloud</span></p>
<!-- intro osp -->
</div>
<div id="section-1" class="slide section level1">
<h1><img src="../images/universe_of/osp-web-full.png" /></h1>
</div>
<div id="modes-of-address" class="slide section level1">
<h1>modes of address</h1>
<p>problematic and conflictual manors in which, we, as users, get addressed by our the services, websites, apps we employ online</p>
<!-- digital ocean awesome popup -->
</div>
<div id="section-2" class="slide section level1">
<h1><img src="../images/grad-pres/digitalocean-awesome.png" /></h1>
<!-- TUMBLR Cards img -->
</div>
<div id="section-3" class="slide section level1">
<h1><img src="../images/grad-pres/nothardtoexplain-crop.png" /></h1>
<!-- WIRED ad block -->
</div>
<div id="section-4" class="slide section level1">
<h1><img src="../images/wired-the-thing-with-ad-blockers.png" /></h1>
</div>
<div id="section-5" class="slide section level1">
<h1><img src="../images/twitter-sounds-good/Screenshot_from_2017-05-18_11-52-15.png" /></h1>
</div>
<div id="section-6" class="slide section level1">
<h1><img src="../images/twitter-sounds-good/twitter-sounds-good.gif" /></h1>
</div>
<div id="section-7" class="slide section level1">
<h1><img src="../images/real-life-mag-mismanaged-heart.png" /></h1>
</div>
<div id="will-davies-the-mismanaged-heart" class="slide section level1">
<h1>Will Davies <em>The mismanaged heart</em>:</h1>
<p><em>«If the function of informality is to erode the distinction between work and leisure, then informal rhetoric is a necessary feature of platforms that want to mediate and capitalize on all aspects of our lives, including work, family, and social life.»</em></p>
</div>
<div id="rather-than-ask-coldly-what-is-your-date-of-birth-platforms-simply-offer-to-help-celebrate-your-birthday" class="slide section level1">
<h1><em>Rather than ask coldly, “What is your date of birth?” platforms simply offer to help “celebrate your birthday!</em>”</h1>
<p>Rather than demand “your full address,” they invite you to identify a certain location as “home.”</p>
<blockquote>
<p>[…]the <strong>rhetorical turn</strong> toward conviviality has also played a critical role, allowing surveillance to be administered and experienced as a form of care. ↳ it’s important to reflect on how this <strong>rhetorical turn</strong> actually works to engage us. ↳ Facebook and Twitter ask: ‘How are you?’ or ‘What is on your mind?’</p>
</blockquote>
<!--
# ![](../images/universe_of/n26-bday.png)
-->
</div>
<div id="renaming-interfaces-as-experiences" class="slide section level1">
<h1>renaming interfaces as experiences</h1>
<h3 id="seamless-interfaces">Seamless interfaces:</h3>
<p>two (or more) computer programs that are carefully joined together so that they appear to be a single program with a single user interface. A seamless interface will conceal the fact that it is created from several different programs.</p>
<h3 id="designing-for-experiences">Designing for experiences</h3>
<ul>
<li>‘experience designers’</li>
<li>‘user experience designers’</li>
<li>‘user interface designers’</li>
<li>‘interaction designers’</li>
<li>‘product designers’</li>
</ul>
<h3 id="language-shifts">Language shifts</h3>
<ul>
<li>Computer → technology</li>
<li>Interface → Experience</li>
<li>Users → People <br><small>Lialina Olia, 2014. Rich User Experience, UX and Desktopization of War. [online] Available at: http://contemporary-home-computing.org/RUE/ [Accessed 10 October 2016]</small></li>
</ul>
</div>
<div id="a-logic-of-totality" class="slide section level1">
<h1>a logic of totality</h1>
<p>Deep integration, language shifts, design for experiences and convivial modes of address culminate in a logic of totality. Totalised spaces are frames that present themselves as all inclusive, all encapsulating spaces that have no outsides, they are everything, they become everything.</p>
</div>
<div id="adversarial-design" class="slide section level1">
<h1>Adversarial design</h1>
<p>Adversarial design is design that does the work of agonism. Agonism is a political theory that emphasizes the potentially positive aspects of certain forms of political conflict. Adversarial interfaces do the work of agonism in multiple ways: it expresses bias and divisive positions; it provides opportunities to participate in disputes over values, believes and desires; and it models alternate socio-material configurations that demonstrate possible futures.</p>
</div>
<div id="adversarial-interfaces-1" class="slide section level1">
<h1>Adversarial Interfaces</h1>
<p><a href="http://adversarial.interfaces.site">adversarial.interfaces.site</a></p>
<p><img src="../images/universe_of/adversarial-interfaces-obj.svg" /></p>
<p>I put adversarial interfaces forwards as a way to point the finger at interface design as a space that needs more critical thinking. I believe that the methods we are given to interact with ear shaping technology, the windows we are given to interact with do not enable proper exploration or understanding of the matters at hand.</p>
<p>An adversarial interface can take most or any form so long as it exists / its purpose is to do the work of agonism for user interfaces.</p>
</div>
<div id="examples-of-adversarial-interfaces" class="slide section level1">
<h1>examples of adversarial interfaces:</h1>
<ul>
<li><a href="">Anti adblock killer</a></li>
<li><a href="http://localhost:8000/adversarial-interfaces/clickbait-stopper.html">clickbait stopper</a></li>
<li><a href="http://silviolorusso.com/work/shouldnt-you-be-working/">Shouldn’t you be working — Silvio Lorusso</a></li>
</ul>
</div>
<div id="website-footprint-joana-moll" class="slide section level1">
<h1>Website footprint — Joana Moll</h1>
<p><img src="../images/universe_of/crit-website-footprint-full.png" /> <img src="../images/universe_of/crit-website-footprint-crop.png" /></p>
</div>
<div id="server-battery-level-low-tech-mag" class="slide section level1">
<h1>Server battery level — Low tech mag</h1>
<p><img src="../images/universe_of/low-tech1.png" /> <img src="../images/universe_of/low-tech2.png" /> <img src="../images/universe_of/low-tech3.png" /></p>
</div>
<div id="main-references-theoretical-scaffold" class="slide section level1">
<h1>Main references — Theoretical scaffold</h1>
<p><img src="../images/universe_of/scaffolding1-obj.svg" /></p>
<!--
# [https://networksofonesown.constantvzw.org/](https://networksofonesown.constantvzw.org/)

-->
<!--
# Talk structure:
* Personal introduction
* Ongoing research during and since PZI
* Examples of conflicting online modes of address
  * Digital ocean
  * tumblr
  * Wired ad blocker blocker
  * Twitter *sounds good*
  * youtube monkeys
  * Ryanair oops
* Will Davies *The mismanaged heart*: *«If the function of informality is to erode the distinction between work and leisure, then informal rhetoric is a necessary feature of platforms that want to mediate and capitalize on all aspects of our lives, including work, family, and social life.»*
* <strike> a collection of —wares ?</strike>
* These collected examples signify to me that this type of informality is unparalled, nowhere else than online would we see such a collection of friendly languages. The tone of this speech, this informal mode of addressing strangers does not benefit the visitor in any way, only attitude is displayed, in a tongue and cheek speech that seems to want to say, *hey buddy, don't worry about a thing we're managing this thing for you, sit back and let us make it as easy as possible for you. Aren't we nice to you ?*
* Google logo redesign example
* In parallel to all of this, my position as a designer has been changing. As a byproduct of exploring back end computing over front end aesthetics I find myself in a kind of a tool curator[ref] position in my commercial work.
[insert ref Sede Gruses, privacy after the agile turn]

I find myself confronted with the my customers / common person's digital literacy, having to make decisions as to what tools to pick to best serve a purpose, but usually that choice is made based on the amount of ease the people I work with expect to have when using my delivered projects.

So I have had this nagging cloud over my head around the topic of literacy of the internet, and the gap in understanding that seems to be growing, not narrowing, as more time is spent online. Now more than ever our lives are shaped by online services, websites apps, our cultures absorb online phenomenon, but the infrastructure and knowledge infrastructure that this all relies on seems to be further away than ever, always online, always connected, always facing a beautiful interface.

To come back to the informal modes of address that our interfaces speak to us in, I believe that one of the effects, besides the data hungry, data is the new oil, Will Davies problematics, is the perpetuation of this idea of the internet being a totality. Totality in this sense meaning that the interfaces / services we deal with as users present the internet as a single full item.

Seamlesness in interface design is one of the practices that confirms this attitude of the user shoudn't have to feel any of the layers they are dealing with. Practices of user interfaces, user experience design also show this. Now don't get me wrong, as a graphic designer, I also appreciate good design being implemented into our apps and services. But when continued, when repeated, when perpetuated, the accumulation of all of these well designed, experience focused apps distance us from what is actually going on in the background which leads us to these very particular error messages. Every now and then, websites do crash, apps to encounter errors and even these pages are designed.

So this idea of a totality is also put forwards by artist Zach Blas.

Carl DiSalvo adversariality in design

Adversarial interfaces

-->
</div>
