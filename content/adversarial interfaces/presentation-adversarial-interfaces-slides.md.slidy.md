Title: slides adversarial interfaces public presentation
Date: 2017/06/15
Template: slidy
Status: published

<div class="slide section level1">

<p>Title: adversarial interfaces public presentation Date: 2017/06/15 Template: slidy Status: draft</p>
</div>
<div id="adversarial-interfaces" class="slide section level1">
<h1>Adversarial interfaces</h1>
<p>http://adversarial.interfaces.site</p>
<p>Graduate reseach project<br> Colm O'Neill<br> mail@colm.be<br></p>
<!-- digital ocean awesome popup -->
</div>
<div id="section" class="slide section level1">
<h1><img src="../images/grad-pres/digitalocean-awesome.png" /></h1>
<!-- TUMBLR Cards img -->
</div>
<div id="section-1" class="slide section level1">
<h1><img src="../images/grad-pres/nothardtoexplain-crop.png" /></h1>
<!-- WIRED ad block -->
</div>
<div id="section-2" class="slide section level1">
<h1><img src="../images/wired-the-thing-with-ad-blockers.png" /></h1>
</div>
<div id="section-3" class="slide section level1">
<h1><img src="../images/twitter-sounds-good/Screenshot_from_2017-05-18_11-52-15.png" /></h1>
</div>
<div id="section-4" class="slide section level1">
<h1><img src="../images/twitter-sounds-good/twitter-sounds-good.gif" /></h1>
</div>
<div id="section-5" class="slide section level1">
<h1><img src="../images/real-life-mag-mismanaged-heart.png" /></h1>
</div>
<div id="section-6" class="slide section level1">
<h1><img class="vertical" src="../images/surveillance-capture.png"></h1>
</div>
<div id="section-7" class="slide section level1">
<h1><img src="../images/grad-pres/facebook.gif" /></h1>
<!-- TEXT SLIDE: Seamless definition -->
</div>
<div id="seamless-interfaces" class="slide section level1">
<h1>Seamless interfaces:</h1>
<p>two (or more) computer programs that are carefully joined together so that they appear to be a single program with a single user interface. A seamless interface will conceal the fact that it is created from several different programs.</p>
</div>
<div id="section-8" class="slide section level1">
<h1><img src="../images/tangible.tools.screen.png" /></h1>
</div>
<div id="section-9" class="slide section level1">
<h1><img src="../images/internet-will-disappear.png" /></h1>
</div>
<div id="section-10" class="slide section level1">
<h1><img class="vertical" src="../images/JK-Gibson-Graham.jpg"></h1>
<!-- adversarial interfaces site -->
</div>
<div id="section-11" class="slide section level1">
<h1><img src="../images/adversarial-interfaces-site.png" /></h1>
<!-- Text Slide: Adversarial Design, Agonism, Chantal Mouffe -->
</div>
<div id="adversarial-design" class="slide section level1">
<h1>Adversarial design</h1>
<p>Adversarial design: (Carl DiSalvo) a type of political design that evokes and engages political issues. Adversarial Design does the work in expressing and enabling agonism.</p>
<p>Agonism: (Chantal Mouffe) a political theory that emphasises the potentially positive aspects of certain forms of political conflic.</p>
</div>
