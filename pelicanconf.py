#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = u"Colm O'Neill"
SITENAME = u'adversarial interfaces'
SITESUBTITLE = u'user interfaces rhetoric reveal its politics / critical perspectives on what user interfaces could be'
SITEURL = 'http://adversarial.interfaces.site'

PATH = 'content'
STATIC_PATHS = ['images']
OUTPUT_PATH = 'public'

TIMEZONE = 'Europe/Paris'

DEFAULT_LANG = u'en'
DEFAULT_DATE_FORMAT = '%d/%m/%Y'
DATE_FORMATS = {
    'en': '%d/%m/%Y',
}

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = 'feeds/all.atom.xml'
CATEGORY_FEED_ATOM = 'feeds/%s.atom.xml'
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = 'feeds/%s.atom.xml'
AUTHOR_FEED_RSS = 'feeds/%s.rss.xml'

DEFAULT_CATEGORY = 'All'
DISPLAY_CATEGORIES_ON_MENU = True
DISPLAY_PAGES_ON_MENU = True

# MENUITEMS = (('about', '/pages/about-this-site.html'),)

THEME = "theme/adversarial.interfaces.site.theme"
CSS_FILE = 'screen.css'

ARTICLE_URL = '{category}/{slug}.html'
ARTICLE_SAVE_AS = '{category}/{slug}.html'
# CATEGORY_URL = '{category}/{slug}.html'
# CATEGORY_SAVE_AS = '{category}/{slug}.html'
# PAGE_SAVE_AS = '{page_name}.html'
# TAG_SAVE_AS = 'tag/{tag_name}.html'

# Uncomment following line if you want document-relative URLs when developing
RELATIVE_URLS = True

PLUGIN_PATHS = ['plugins/pelican-toc', 'plugins/neighbors', 'plugins/simple_footnotes', 'plugins/pelican_youtube']
PLUGINS = ['toc', 'neighbors', 'simple_footnotes', 'pelican_youtube']
